module oldController(start, clk, rst, ldx, ldy, clr, ldz, sel1, sel2);
  input start, clk, rst;
  output ldx, ldy, clr, ldz, sel1, sel2;
  
  wire v0, v1, v2, u0, u1, u2, vb0, vb1, vb2;
  wire v1vb0, v2vb0, startvb1vb0, vb2vb1v0, v1v0;
  //wire andsel;
  
  //combinatoinal
  not m1(vb1, v1);
  not m2(vb2, v2);
  not m9(vb0 ,v0);
   
  and m3(ldx, v0, vb1, vb2);
  and m4(ldy, v0, vb1, vb2);
  and m5(clr, v0, vb1, vb2);
  and m7(sel1, v0, ldz);
  and m8(sel2, v2, vb1);
  
  xor m6(ldz, v1, v2);
  
  and m10(v1vb0, v1, vb0);
  and m11(v2vb0, v2, vb0);
  and m12(startvb1vb0, start, vb1, vb0);
  or m13(u0, v1vb0, v2vb0, startvb1vb0);
  
  and m14(vb2vb1v0, vb2, vb1, v0);
  or m15(u1, v1vb0, vb2vb1v0);
  
  and m16(v1v0, v1, v0);
  or m17(u2, v1v0, v2vb0);
  //sequential
  DFFwithLd D0(.parIn(u0), .clk(clk), .rst(rst), .ld(1'b1) , .q(v0));
  DFFwithLd D1(.parIn(u1), .clk(clk), .rst(rst), .ld(1'b1) , .q(v1));
  DFFwithLd D2(.parIn(u2), .clk(clk), .rst(rst), .ld(1'b1) , .q(v2));
endmodule
