module DFFEightBitWithLoadAndClear(parIn , rst , clk , clr , ld , q);
	output[7:0] q;
	input[7:0] parIn;
	input rst , clk , clr , ld;
	
	DFFwithLdAndClr dff1 (.parIn(parIn[0]) , .rst(rst) , .clk(clk) , .ld(ld) , .clr(clr) , .q(q[0]) );
	DFFwithLdAndClr dff2 (.parIn(parIn[1]) , .rst(rst) , .clk(clk) , .ld(ld) , .clr(clr) , .q(q[1]) );
	DFFwithLdAndClr dff3 (.parIn(parIn[2]) , .rst(rst) , .clk(clk) , .ld(ld) , .clr(clr) , .q(q[2]) );
	DFFwithLdAndClr dff4 (.parIn(parIn[3]) , .rst(rst) , .clk(clk) , .ld(ld) , .clr(clr) , .q(q[3]) );
	DFFwithLdAndClr dff5 (.parIn(parIn[4]) , .rst(rst) , .clk(clk) , .ld(ld) , .clr(clr) , .q(q[4]) );
	DFFwithLdAndClr dff6 (.parIn(parIn[5]) , .rst(rst) , .clk(clk) , .ld(ld) , .clr(clr) , .q(q[5]) );
	DFFwithLdAndClr dff7 (.parIn(parIn[6]) , .rst(rst) , .clk(clk) , .ld(ld) , .clr(clr) , .q(q[6]) );
	DFFwithLdAndClr dff8 (.parIn(parIn[7]) , .rst(rst) , .clk(clk) , .ld(ld) , .clr(clr) , .q(q[7]) );
endmodule
