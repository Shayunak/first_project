module DFFFourBitWithLd(parIn , clk , rst , ld , q);
	output [3:0] q;
	input[3:0] parIn;
	input rst,clk,ld;
	
	DFFwithLd dff1 (.parIn(parIn[0]) , .clk(clk) , .rst(rst) , .ld(ld) , .q(q[0]) );
	DFFwithLd dff2 (.parIn(parIn[1]) , .clk(clk) , .rst(rst) , .ld(ld) , .q(q[1]) );
	DFFwithLd dff3 (.parIn(parIn[2]) , .clk(clk) , .rst(rst) , .ld(ld) , .q(q[2]) );
	DFFwithLd dff4 (.parIn(parIn[3]) , .clk(clk) , .rst(rst) , .ld(ld) , .q(q[3]) );
	
endmodule
