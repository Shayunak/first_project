module DFFModules(d , clk , clr , q);
	input clk,clr,d;
	output reg q;
	always@(posedge clk , posedge clr) begin
		if(clr) q <= 1'b0;
		else q <= d;
	end
endmodule
