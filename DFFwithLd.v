module DFFwithLd(parIn , clk , rst , ld , q);
	input clk,rst,ld,parIn;
	output q;
	S1 dff (.out(q) , .D00(q) , .D01(1'b0) , .D10(parIn) , .D11(1'b0) 
		, .A1(ld) , .B1(1'b0) , .A0(1'b0) , .CLR(rst) , .CLK(clk) );
endmodule
