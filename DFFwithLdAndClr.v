module DFFwithLdAndClr(parIn , clk , rst , ld , clr , q);
	input clk,rst,ld,parIn,clr;
	output q;
	S2 dff (.out(q) , .D00(q) , .D01(1'b0) , .D10(parIn) , .D11(1'b0) 
	, .A1(ld) , .B1(1'b0) , .A0(clr) , .B0(1'b1) , .CLR(rst) , .CLK(clk) );
endmodule
