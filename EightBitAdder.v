module EightBitAdder(inp0, inp1, cin, sum, cout);
  input [7:0] inp0, inp1;
  input cin;
  output [7:0] sum;
  output cout;
  wire w0, w1, w2, w3, w4, w5, w6;
  
  FullAdder F0(.inp0(inp0[0]), .inp1(inp1[0]), .cin(cin), .sum(sum[0]), .cout(w0));
  FullAdder F1(.inp0(inp0[1]), .inp1(inp1[1]), .cin(w0), .sum(sum[1]), .cout(w1));
  FullAdder F2(.inp0(inp0[2]), .inp1(inp1[2]), .cin(w1), .sum(sum[2]), .cout(w2));
  FullAdder F3(.inp0(inp0[3]), .inp1(inp1[3]), .cin(w2), .sum(sum[3]), .cout(w3));
  FullAdder F4(.inp0(inp0[4]), .inp1(inp1[4]), .cin(w3), .sum(sum[4]), .cout(w4));
  FullAdder F5(.inp0(inp0[5]), .inp1(inp1[5]), .cin(w4), .sum(sum[5]), .cout(w5));
  FullAdder F6(.inp0(inp0[6]), .inp1(inp1[6]), .cin(w5), .sum(sum[6]), .cout(w6));
  FullAdder F7(.inp0(inp0[7]), .inp1(inp1[7]), .cin(w6), .sum(sum[7]), .cout(cout));
endmodule
