module C1(F , A0 , A1 , SA , B0 , B1 , SB , S0 , S1);
	output F;
	input A0 , A1 , SA , B0 , B1 , SB , S0 , S1;
	wire S2, F1, F2;
	or or2(S2, S0, S1);
	MUXTwoToOneOneBit muxGateLevel1(.inp0(A0) , .inp1(A1) , .sel(SA) , .out(F1));
	MUXTwoToOneOneBit muxGateLevel2(.inp0(B0) , .inp1(B1) , .sel(SB) , .out(F2));
	MUXTwoToOneOneBit muxGateLevel3(.inp0(F1) , .inp1(F2) , .sel(S2) , .out(F));
endmodule

module C2(out , D00 , D01 , D10 , D11 , A1 , B1 , A0 , B0);
	output out;
	input D00 , D01 , D10 , D11 , A1 , B1 , A0 , B0;
	wire S1,S0;
	or or1 (S1 , A1 , B1);
	and and1 (S0 , A0 , B0);
	MUXFourToOneOneBit muxGateLevel (.inp0(D00) , .inp1(D01) , .inp2(D10) , .inp3(D11) , .sel0(S0) , .sel1(S1) , .out(out) );
endmodule

module S1(out , D00 , D01 , D10 , D11 , A1 , B1 , A0 , CLR , CLK);
	output out;
	input D00 , D01 , D10 , D11 , A1 , B1 , A0 , CLR , CLK;
	wire S1, S0, D;
	or or1(S1, A1, B1);
	and and1(S0, A0, CLR);
	MUXFourToOneOneBit muxGateLevel (.inp0(D00) , .inp1(D01) , .inp2(D10) , .inp3(D11) , .sel0(S0) , .sel1(S1) , .out(D) );
	DFFModules dff (.d(D) , .clk(CLK), .clr(CLR) , .q(out) );
endmodule

module S2(out , D00 , D01 , D10 , D11 , A1 , B1 , A0 , B0 , CLR , CLK);
	output out;
	input D00 , D01 , D10 , D11 , A1 , B1 , A0 , B0 , CLR , CLK;
	wire S1,S0,d;
	or or1 (S1 , A1 , B1);
	and and1 (S0 , A0 , B0);
	MUXFourToOneOneBit muxGateLevel (.inp0(D00) , .inp1(D01) , .inp2(D10) , .inp3(D11) , .sel0(S0) , .sel1(S1) , .out(d) );
	DFFModules dff (.d(d) , .clk(CLK), .clr(CLR) , .q(out) );
endmodule
