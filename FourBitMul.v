module FourBitMul(start, clk, rst, A, B, result);
  input start, clk, rst; 
  input[3:0] A, B;
  output[7:0] result;
  
  wire[3:0] a, b, res;
  wire ldx, ldy, ldz, sel1, sel2, clr, temp;
  wire[1:0] op1, op2;
  wire[7:0] op3,op4;
  
  DFFFourBitWithLd inp1(.parIn(A), .clk(clk), .rst(rst), .ld(ldx) , .q(a));
  DFFFourBitWithLd inp2(.parIn(B), .clk(clk), .rst(rst), .ld(ldy) , .q(b));
  
  MUXTwoToOneTwoBit mux1(.inp0(a[1:0]), .inp1(a[3:2]), .sel(sel1), .out(op1));
  MUXTwoToOneTwoBit mux2(.inp0(b[1:0]), .inp1(b[3:2]), .sel(sel2), .out(op2));
  
  MultiplierTwoInTwo mul2in2(.A(op1), .B(op2), .C(res));
  
  MUXFourToOneEightBit mux3(.inp0({4'b0000, res}), .inp1({2'b00, res, 2'b00}), .inp2({2'b00, res, 2'b00}), .inp3({res, 4'b0000}), 
                            .sel0(sel1) , .sel1(sel2), .out(op3));
                            
  EightBitAdder adder(.inp0(op3), .inp1(result), .cin(1'b0), .sum(op4), .cout(temp));
  
  DFFEightBitWithLoadAndClear out(.parIn(op4), .rst(rst), .clk(clk), .clr(clr), .ld(ldz), .q(result));
  
  Controller cu(.start(start), .clk(clk), .rst(rst), .ldx(ldx), .ldy(ldy), .clr(clr), .ldz(ldz), .sel1(sel1), .sel2(sel2));
endmodule
