module FullAdder(inp0, inp1, cin, sum, cout);
  input inp0, inp1, cin;
  output sum, cout;
  wire w1, w2, w3;    
  
  C2 firstXorC2 (.out(w1) , .D00(1'b0) , .D01(1'b0) , .D10(1'b1) , .D11(1'b0) 
	, .A1(inp1) , .B1(inp0) , .A0(inp0) , .B0(inp1) );

  C1 firstAndC1 (.F(w2) , .A0(1'b0) , .A1(1'b0) , .SA(1'b0) 
	, .B0(1'b0) , .B1(inp0) , .SB(1'b1) , .S0(1'b0) , .S1(inp1) );

  C1 secondAndC1 (.F(w3) , .A0(1'b0) , .A1(1'b0) , .SA(1'b0) 
	, .B0(1'b0) , .B1(w1) , .SB(1'b1) , .S0(1'b0) , .S1(cin) );

  C1 orC1 (.F(cout) , .A0(1'b0) , .A1(1'b0) , .SA(1'b0) , .B0(1'b1) 
	, .B1(1'b0) , .SB(1'b0) , .S0(w2) , .S1(w3) );

  C2 secondXorC2 (.out(sum) , .D00(1'b0) , .D01(1'b0) , .D10(1'b1) , .D11(1'b0) 
	, .A1(cin) , .B1(w1) , .A0(w1) , .B0(cin) );
endmodule
