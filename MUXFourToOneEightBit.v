module MUXFourToOneEightBit(inp0 , inp1 , inp2 , inp3 , sel0 , sel1 , out);
	input[7:0] inp0,inp1,inp2,inp3;
	input sel0,sel1;
	output[7:0] out;

	MuxFour mux1 (.inp0(inp0[0]) , .inp1(inp1[0]) , .inp2(inp2[0]) , .inp3(inp3[0]) , .sel0(sel0) , .sel1(sel1) , .out(out[0]) );
	MuxFour mux2 (.inp0(inp0[1]) , .inp1(inp1[1]) , .inp2(inp2[1]) , .inp3(inp3[1]) , .sel0(sel0) , .sel1(sel1) , .out(out[1]) );
	MuxFour mux3 (.inp0(inp0[2]) , .inp1(inp1[2]) , .inp2(inp2[2]) , .inp3(inp3[2]) , .sel0(sel0) , .sel1(sel1) , .out(out[2]) );
	MuxFour mux4 (.inp0(inp0[3]) , .inp1(inp1[3]) , .inp2(inp2[3]) , .inp3(inp3[3]) , .sel0(sel0) , .sel1(sel1) , .out(out[3]) );
	MuxFour mux5 (.inp0(inp0[4]) , .inp1(inp1[4]) , .inp2(inp2[4]) , .inp3(inp3[4]) , .sel0(sel0) , .sel1(sel1) , .out(out[4]) );
	MuxFour mux6 (.inp0(inp0[5]) , .inp1(inp1[5]) , .inp2(inp2[5]) , .inp3(inp3[5]) , .sel0(sel0) , .sel1(sel1) , .out(out[5]) );
	MuxFour mux7 (.inp0(inp0[6]) , .inp1(inp1[6]) , .inp2(inp2[6]) , .inp3(inp3[6]) , .sel0(sel0) , .sel1(sel1) , .out(out[6]) );
	MuxFour mux8 (.inp0(inp0[7]) , .inp1(inp1[7]) , .inp2(inp2[7]) , .inp3(inp3[7]) , .sel0(sel0) , .sel1(sel1) , .out(out[7]) );
endmodule 
