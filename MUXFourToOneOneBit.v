module MUXFourToOneOneBit(inp0 , inp1 , inp2 , inp3 , sel1 , sel0 , out);
	input inp0,inp1,inp2,inp3,sel0,sel1;
	output out;
	wire sel0Inv,sel1Inv;
	wire T0,T1,T2,T3;

	not not0 (sel0Inv , sel0);
	not not1 (sel1Inv , sel1);
	and and0 (T0 , sel0Inv , sel1Inv , inp0);
	and and1 (T1 , sel0 , sel1Inv , inp1);
	and and2 (T2 , sel0Inv , sel1 , inp2);
	and and3 (T3 , sel0 , sel1 , inp3);
	or orFin (out , T0 , T1 , T2 , T3);
endmodule
