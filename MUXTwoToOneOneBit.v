module MUXTwoToOneOneBit(inp0 , inp1 , sel , out);
	input inp0,inp1,sel;
	output out;
	wire selInv,OneCaseOut,ZeroCaseOut;
	
	not not0(selInv , sel);
	and and0(ZeroCaseOut , selInv , inp0);
	and and1(OneCaseOut , sel , inp1);
	or orFin(out , OneCaseOut , ZeroCaseOut);
endmodule
