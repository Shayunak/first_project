module MUXTwoToOneTwoBit(inp0 , inp1 , sel , out);
	input [1:0]inp0,inp1;
	input sel;
	output [1:0]out;
	
	MuxTwo mux1 (.inp0(inp0[0]) , .inp1(inp1[0]) , .sel(sel) , .out(out[0]) );
	MuxTwo mux2 (.inp0(inp0[1]) , .inp1(inp1[1]) , .sel(sel) , .out(out[1]) );
endmodule
