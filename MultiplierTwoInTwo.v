module oldMultiplierTwoInTwo(A , B , C);
	input[1:0] A,B;
	output[3:0] C;
	wire[1:0] invA,invB;
	wire and0C2 , and1C2;
	wire and0C1 , and1C1, and2C1,and3C1;
	//inverted
	not not0 (invA[0] , A[0]);
	not not1 (invA[1] , A[1]);
	not not2 (invB[0] , B[0]);
	not not3 (invB[1] , B[1]);
	//c0
	and andC0 (C[0] , A[0] , B[0]);
	//c1
	and andC1_1 (and0C1 , A[1] , B[0] , invB[1]);
	and andC1_2 (and1C1 , A[0] , B[1] , invB[0]);
	and andC1_3 (and2C1 , A[0] , B[1] , invA[1]);
	and andC1_4 (and3C1 , A[1] , B[0] , invA[0]);
	or orFinC1 (C[1] , and0C1 , and1C1 , and2C1 , and3C1);
	//c2
	and andC2_1 (and0C2 , A[1] , B[1] , invA[0]);
	and andC2_2 (and1C2 , A[1] , B[1] , invB[0]);
	or orFinC2 (C[2] , and0C2 , and1C2);
	//c3
	and andC3 (C[3] , A[0] , A[1] , B[0] , B[1]);
endmodule
