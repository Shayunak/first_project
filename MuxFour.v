module MuxFour(inp0 , inp1 , inp2 , inp3 , sel1 , sel0 , out);
	input inp0,inp1,inp2,inp3,sel0,sel1;
	output out;
	C1 mux (.F(out) , .A0(inp0) , .A1(inp1) , .SA(sel0) , .B0(inp2)
	 , .B1(inp3) , .SB(sel0) , .S0(sel1) , .S1(1'b0) );
endmodule
