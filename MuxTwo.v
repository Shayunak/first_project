module MuxTwo(inp0 , inp1 , sel , out);
	input inp0,inp1,sel;
	output out;
	C1 mux (.F(out) , .A0(inp0) , .A1(inp1) , .SA(sel)
	 , .B0(1'b0) , .B1(1'b0) , .SB(1'b0) , .S0(1'b0) , .S1(1'b0) );
endmodule

