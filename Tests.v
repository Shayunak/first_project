module DFFEightBitTB();
	reg clk,rst,clr,ld;
	reg [7:0] inp;
	wire [7:0] out;
	integer i;
	DFFEightBitWithLoadAndClear cut (.clk(clk) , .rst(rst) , .clr(clr) , .ld(ld) , .parIn(inp) , .q(out));
	initial begin
		clk = 1'b0;
		#10 rst = 1'b1;
		#10 rst = 1'b0; #10
		repeat(1000) #50 clk = ~clk; 
	end
	initial begin
		#25
		ld = 1;
		clr = 0;
		for(i = 0 ; i < 256 ; i = i + 1) begin
			inp = i;
			#100
			if(out != inp)
				$display("This output value %b is not the same as input" , out);
		end
		#500
		clr = 1'b1;
		#100
		if(out != 8'b0)
			$display("Not successful clearing");
		clr = 1'b0;
		$stop;
	end
endmodule

module MUXFourbitTB();
	reg [7:0]inp[0:3];
	reg [1:0]sel;
	wire [7:0]out;
	integer i;
	MUXFourToOneEightBit cut (.inp0(inp[0]) , .inp1(inp[1]) , .inp2(inp[2]) , .inp3(inp[3]) , .sel0(sel[0]) , .sel1(sel[1]) , .out(out) );
	initial begin
		inp[0] = 8'b0001_1111; inp[1] = 8'b1111_0101; inp[2] = 8'b1010_1010; inp[3] = 8'b0110_0110;
		for(i = 0 ; i < 4 ; i = i + 1) begin
			sel = i[1:0]; #20
			if(out != inp[i])
				$display("Not valid output for input : %b" , inp[i]);
		end
		$stop;
	end
endmodule

module MultTwoTB();
	reg[1:0] A,B;
	wire[3:0] C;
	integer i,j;
	MultiplierTwoInTwo cut (.A(A) , .B(B) , .C(C) );
	initial begin
		for(i = 0 ; i <= 3 ; i = i + 1) begin
			for(j = 0 ; j <= 3 ; j = j + 1) begin
				A = i; B = j; #10
				if(C != i * j)
					$display("invalid output %b for input %b and %b" , C , i , j);
			end
		end
		$stop;
	end
endmodule

module AdderTB();
  reg [7:0] A, B;
  reg cin;
  wire [7:0] S;
  wire cout;
  
  EightBitAdder adder(.inp0(A), .inp1(B), .cin(cin), .sum(S), .cout(cout));
  
  initial begin
    cin = 1'b0;
    A = 8'b00001010;
    B = 8'b00000110;
    #100;
    A = 8'b00011111;
    B = 8'b00001001;    
    #100;
    $stop;
  end
endmodule

module ControllerTB();
    reg clk, rst, start;
    wire ldx, ldy, clr, ldz, sel1, sel2;
    
    Controller cu(.start(start), .clk(clk), .rst(rst), .ldx(ldx), .ldy(ldy), .clr(clr), .ldz(ldz), .sel1(sel1), .sel2(sel2));
    initial begin
      clk = 0;
      repeat(20) #25 clk = ~clk;
    end
    
    initial begin
      rst = 1;
      start = 0;
      #50;
      rst = 0;
      #50		
      start = 1;
      #100
      start = 0;
    end
endmodule

module finalTB();
	reg start,clk,rst;
	reg[3:0] A,B;
	wire[7:0] C;
	integer i,j;
	FourBitMul mul (.start(start) , .A(A) , .B(B) , .result(C) , .clk(clk) , .rst(rst) );
	initial begin
		clk = 1'b0;
		start = 1'b0;
		repeat(10000) #25 clk = ~clk;
	end
	initial begin
		rst = 1'b1;
		#50
		rst = 1'b0;
		for(i = 0 ; i < 16 ; i = i + 1) begin
			for(j = 0 ; j < 16 ; j = j + 1) begin
				#500
				A = i; B = j; start = 1'b1;
				#50
				start = 1'b0;
				#300
				if(C != A*B)
					$display("Invalid output for %d with inputs: %d %d" , C , A , B);
			end
		end
	end
endmodule




