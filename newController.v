module Controller(start, clk, rst, ldx, ldy, clr, ldz, sel1, sel2);
  input start, clk, rst;
  output ldx, ldy, clr, ldz, sel1, sel2;
  wire v0, v1, v2;
  wire t1, t2, t3;
  
  S2 s20(.out(v2) , .D00(v2) , .D01(v1) , .D10(1'b0) , .D11(1'b0) , .A1(1'b0) , .B1(1'b0) , .A0(v0) , .B0(1'b1) , .CLR(rst) , .CLK(clk));
  
  C1 c10(.F(t1) , .A0(1'b0) , .A1(1'b0) , .SA(1'b0) , .B0(1'b1) , .B1(1'b0) , .SB(v0) , .S0(v2) , .S1(v1));
  C1 c11(.F(t2) , .A0(start) , .A1(1'b0) , .SA(1'b0) , .B0(1'b0) , .B1(1'b0) , .SB(1'b0) , .S0(v0) , .S1(v1));
  S1 s10(.out(v0) , .D00(1'b0) , .D01(1'b0) , .D10(1'b1) , .D11(1'b0) , .A1(t1) , .B1(t2) , .A0(1'b0) , .CLR(rst) , .CLK(clk));
  
  C1 c12(.F(t3) , .A0(1'b0) , .A1(1'b1) , .SA(1'b1) , .B0(1'b0) , .B1(1'b0) , .SB(1'b0) , .S0(v1) , .S1(v2));
  S2 s21(.out(v1) , .D00(1'b0) , .D01(v1) , .D10(1'b0) , .D11(t3) , .A1(v0) , .B1(1'b0) , .A0(1'b1) , .B0(1'b1) , .CLR(rst) , .CLK(clk));
  
  C2 xor1(.out(ldz) , .D00(1'b0) , .D01(1'b0) , .D10(1'b1) , .D11(1'b0) , .A1(v2) , .B1(v1) , .A0(v1) , .B0(v2));
  
  C1 c13(.F(sel1) , .A0(1'b0) , .A1(1'b0) , .SA(1'b0) , .B0(1'b0) , .B1(1'b1) , .SB(ldz) , .S0(1'b0) , .S1(v0));
  
  C1 c14(.F(sel2) , .A0(1'b0) , .A1(1'b1) , .SA(v2) , .B0(1'b0) , .B1(1'b0) , .SB(1'b0) , .S0(1'b0) , .S1(v1));
  
  C1 c15(.F(ldx) , .A0(1'b0) , .A1(1'b0) , .SA(1'b0) , .B0(1'b0) , .B1(1'b1) , .SB(t3) , .S0(1'b0) , .S1(v0));
  
  assign clr = ldx;
  assign ldy = ldx;
endmodule
