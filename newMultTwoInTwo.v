module MultiplierTwoInTwo(A , B , C);
	input[1:0] A,B;
	output[3:0] C;
	//C0
  	C1 andC1 (.F(C[0]) , .A0(1'b0) , .A1(1'b0) ,
		 .SA(1'b0) , .B0(1'b0) , .B1(B[0]) , .SB(1'b1) , .S0(1'b0) , .S1(A[0]) );
	//C1
	C2 firstC2 (.out(term1) , .D00(1'b0) , .D01(1'b0) , .D10(A[1]) , 
		.D11(1'b0) , .A1(B[0]) , .B1(1'b0) , .A0(A[0]) , .B0(B[1]) );
	C2 secondC2 (.out(term2) , .D00(1'b0) , .D01(1'b0) , .D10(A[0]) , 
		.D11(1'b0) , .A1(B[1]) , .B1(1'b0) , .A0(A[1]) , .B0(B[0]));
	C1 orC1 (.F(C[1]) , .A0(1'b0) , .A1(1'b0) ,
		 .SA(1'b0) , .B0(1'b1) , .B1(1'b0) , .SB(1'b0) , .S0(term1) , .S1(term2));
	//C2
	C2 c2(.out(C[2]) , .D00(1'b0) , .D01(1'b0) , .D10(A[1]) , .D11(1'b0) , .A1(1'b0) , .B1(B[1]) , .A0(A[0]) , .B0(B[0]));
	//C3
	C2 c3(.out(C[3]) , .D00(1'b0) , .D01(1'b0) , .D10(1'b0) , .D11(A[1]) , .A1(1'b0) , .B1(B[1]) , .A0(A[0]) , .B0(B[0]));
endmodule
